/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forma;

import forma.Forma;

/**
 *
 * @author Alma Rosa Rodriguez Santos
 */
public class PruebaFormas {
    
    public static void main(String... args) {
        Forma miforma = new Rectangulo();
        miforma.dibujar();
        miforma.redimensionar();
    }
}
